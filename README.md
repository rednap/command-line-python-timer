# Command Line Python Timer  
  
Counts down from HH:MM:SS and then blows a little horn

1. edit timer.py and change line 31  
```audio_file = "/path/to/cuca.wav" #CHANGE TO THE LOCATION OF SOUND FILE```  
  
2. copy the sound file to the desired location  
  
3. change the name of the py file to "timer" and chmod the file to executable  
```$ mv timer.py timer && chmod a+x timer```  
  
4. copy the py file to somewhere in your PATH  
```$ cp timer /usr/local/bin```  
  
5. invoke the timer    
```$ timer -h``` # show timer command help   
```$ timer 5``` # set timer for 5 seconds  
```$ timer 5:00``` # set timer for 5 minutes
